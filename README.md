Reproduction: Spring Batch & LocalDate(Time) Job Parameters
===========================================================

This project demonstrates that currently, Spring Batch is unable to persist LocalDate(Time) job parameters due to
missing Converter instances.

Setup
-----

* Spring project generated with [Spring Initializer](https://start.spring.io/#!type=maven-project&language=java&platformVersion=3.0.0&packaging=jar&jvmVersion=17&groupId=com.example&artifactId=spring-batch-localdate-reproducer&name=spring-batch-localdate-reproducer&description=Reproducer%20for%20LocalDate(Time)%20JobParameters%20not%20being%20able%20to%20be%20persisted&packageName=com.example.reproducer&dependencies=batch,h2)
  * Spring Batch
  * H2 Database
* Java 17
* macOS 13.0.1

### Exact Version Information
```
$ java -version
openjdk version "17.0.5" 2022-10-18
OpenJDK Runtime Environment Temurin-17.0.5+8 (build 17.0.5+8)
OpenJDK 64-Bit Server VM Temurin-17.0.5+8 (build 17.0.5+8, mixed mode)

$ uname -a
Darwin bytesized 22.1.0 Darwin Kernel Version 22.1.0: Sun Oct  9 20:15:09 PDT 2022; root:xnu-8792.41.9~2/RELEASE_ARM64_T6000 arm64
```

### Code

All code is located in `SpringBatchLocaldateReproducerApplication.java`.
I've also added temporal specifiers to hint at how the code flow progresses.


How to run
----------
 
```shell
$ ./mvnw clean compile spring-boot:run
```

### Observed Output

```
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v3.0.0)

2022-12-12T00:06:09.768+01:00  INFO 22038 --- [           main] pringBatchLocaldateReproducerApplication : Starting SpringBatchLocaldateReproducerApplication using Java 17.0.5 with PID 22038 (/Users/thomas/Downloads/spring-batch-localdate-reproducer/target/classes started by thomas in /Users/thomas/Downloads/spring-batch-localdate-reproducer)
2022-12-12T00:06:09.770+01:00  INFO 22038 --- [           main] pringBatchLocaldateReproducerApplication : No active profile set, falling back to 1 default profile: "default"
2022-12-12T00:06:09.993+01:00  INFO 22038 --- [           main] trationDelegate$BeanPostProcessorChecker : Bean 'springBatchConfiguration' of type [com.example.reproducer.SpringBatchConfiguration$$SpringCGLIB$$0] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
2022-12-12T00:06:10.001+01:00  INFO 22038 --- [           main] trationDelegate$BeanPostProcessorChecker : Bean 'org.springframework.boot.autoconfigure.jdbc.DataSourceConfiguration$Hikari' of type [org.springframework.boot.autoconfigure.jdbc.DataSourceConfiguration$Hikari] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
2022-12-12T00:06:10.009+01:00  INFO 22038 --- [           main] trationDelegate$BeanPostProcessorChecker : Bean 'spring.datasource-org.springframework.boot.autoconfigure.jdbc.DataSourceProperties' of type [org.springframework.boot.autoconfigure.jdbc.DataSourceProperties] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
2022-12-12T00:06:10.016+01:00  INFO 22038 --- [           main] trationDelegate$BeanPostProcessorChecker : Bean 'dataSource' of type [com.zaxxer.hikari.HikariDataSource] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
2022-12-12T00:06:10.018+01:00  INFO 22038 --- [           main] trationDelegate$BeanPostProcessorChecker : Bean 'org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration$JdbcTransactionManagerConfiguration' of type [org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration$JdbcTransactionManagerConfiguration] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
2022-12-12T00:06:10.020+01:00  INFO 22038 --- [           main] trationDelegate$BeanPostProcessorChecker : Bean 'org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration' of type [org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
2022-12-12T00:06:10.022+01:00  INFO 22038 --- [           main] trationDelegate$BeanPostProcessorChecker : Bean 'spring.transaction-org.springframework.boot.autoconfigure.transaction.TransactionProperties' of type [org.springframework.boot.autoconfigure.transaction.TransactionProperties] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
2022-12-12T00:06:10.023+01:00  INFO 22038 --- [           main] trationDelegate$BeanPostProcessorChecker : Bean 'platformTransactionManagerCustomizers' of type [org.springframework.boot.autoconfigure.transaction.TransactionManagerCustomizers] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
2022-12-12T00:06:10.025+01:00  INFO 22038 --- [           main] trationDelegate$BeanPostProcessorChecker : Bean 'transactionManager' of type [org.springframework.jdbc.support.JdbcTransactionManager] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
2022-12-12T00:06:10.026+01:00  INFO 22038 --- [           main] trationDelegate$BeanPostProcessorChecker : Bean 'spring.batch-org.springframework.boot.autoconfigure.batch.BatchProperties' of type [org.springframework.boot.autoconfigure.batch.BatchProperties] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
2022-12-12T00:06:10.027+01:00  INFO 22038 --- [           main] trationDelegate$BeanPostProcessorChecker : Bean 'org.springframework.boot.autoconfigure.batch.BatchAutoConfiguration$SpringBootBatchConfiguration' of type [org.springframework.boot.autoconfigure.batch.BatchAutoConfiguration$SpringBootBatchConfiguration] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
2022-12-12T00:06:10.028+01:00  INFO 22038 --- [           main] trationDelegate$BeanPostProcessorChecker : Bean 'jobRegistry' of type [org.springframework.batch.core.configuration.support.MapJobRegistry] is not eligible for getting processed by all BeanPostProcessors (for example: not eligible for auto-proxying)
2022-12-12T00:06:10.038+01:00  INFO 22038 --- [           main] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Starting...
2022-12-12T00:06:10.108+01:00  INFO 22038 --- [           main] com.zaxxer.hikari.pool.HikariPool        : HikariPool-1 - Added connection conn0: url=jdbc:h2:mem:42bf5817-9934-44d5-a83a-47121189a108 user=SA
2022-12-12T00:06:10.109+01:00  INFO 22038 --- [           main] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Start completed.
2022-12-12T00:06:10.201+01:00  INFO 22038 --- [           main] pringBatchLocaldateReproducerApplication : Started SpringBatchLocaldateReproducerApplication in 0.58 seconds (process running for 0.77)
2022-12-12T00:06:10.223+01:00  INFO 22038 --- [           main] o.s.b.c.l.support.SimpleJobLauncher      : Job: [SimpleJob: [name=firstJob]] launched with the following parameters: [{'date':'{value=Mon Dec 12 00:06:10 CET 2022, type=class java.util.Date, identifying=true}'}]
2022-12-12T00:06:10.235+01:00  INFO 22038 --- [           main] o.s.batch.core.job.SimpleStepHandler     : Executing step: [firstStep]
Hello, world!
2022-12-12T00:06:10.240+01:00  INFO 22038 --- [           main] o.s.batch.core.step.AbstractStep         : Step: [firstStep] executed in 5ms
2022-12-12T00:06:10.244+01:00  INFO 22038 --- [           main] o.s.b.c.l.support.SimpleJobLauncher      : Job: [SimpleJob: [name=firstJob]] completed with the following parameters: [{'date':'{value=Mon Dec 12 00:06:10 CET 2022, type=class java.util.Date, identifying=true}'}] and the following status: [COMPLETED] in 14ms
SUCCESS: {'date':'{value=Mon Dec 12 00:06:10 CET 2022, type=class java.util.Date, identifying=true}'}
FAILED: {'localDate':'{value=2022-12-12, type=class java.time.LocalDate, identifying=true}'}, error was: No converter found capable of converting from type [java.time.LocalDate] to type [java.lang.String]
FAILED: {'localDateTime':'{value=2022-12-12T00:06:10.202517, type=class java.time.LocalDateTime, identifying=true}'}, error was: No converter found capable of converting from type [java.time.LocalDateTime] to type [java.lang.String]
2022-12-12T00:06:10.253+01:00  INFO 22038 --- [ionShutdownHook] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Shutdown initiated...
2022-12-12T00:06:10.256+01:00  INFO 22038 --- [ionShutdownHook] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Shutdown completed.
```

### Important Error Lines

```
SUCCESS: {'date':'{value=Mon Dec 12 00:06:10 CET 2022, type=class java.util.Date, identifying=true}'}
FAILED: {'localDate':'{value=2022-12-12, type=class java.time.LocalDate, identifying=true}'}, error was: No converter found capable of converting from type [java.time.LocalDate] to type [java.lang.String]
FAILED: {'localDateTime':'{value=2022-12-12T00:06:10.202517, type=class java.time.LocalDateTime, identifying=true}'}, error was: No converter found capable of converting from type [java.time.LocalDateTime] to type [java.lang.String]
```

These clearly show that we can't persist LocalDate(Time) job parameters.
