package com.example.reproducer;

import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.support.JobRegistryBeanPostProcessor;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * <0> Provide a Spring Application as usual.
 */
@SpringBootApplication
public class SpringBatchLocaldateReproducerApplication {
    /**
     * <4> Run the Spring Boot application like any other.
     *
     * @param args Application arguments.
     */
    public static void main(String[] args) {
        SpringApplication.run(SpringBatchLocaldateReproducerApplication.class, args);
    }
}


/**
 * <1> Configure Spring Batch to eagerly discover jobs so we can control their execution later.
 */
@Configuration
class SpringBatchConfiguration {
    @Bean
    public JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor(JobRegistry jobRegistry) {
        JobRegistryBeanPostProcessor postProcessor = new JobRegistryBeanPostProcessor();
        postProcessor.setJobRegistry(jobRegistry);
        return postProcessor;
    }
}

/**
 * <2> Provide a simple Hello World Batch Job.
 */
@Configuration
class SimpleBatchJobConfiguration {
    private final JobRepository jobRepository;
    private final PlatformTransactionManager platformTransactionManager;

    public SimpleBatchJobConfiguration(
            JobRepository jobRepository,
            PlatformTransactionManager platformTransactionManager
    ) {
        this.jobRepository = jobRepository;
        this.platformTransactionManager = platformTransactionManager;
    }

    @Bean
    Job firstJob() {
        return new JobBuilder("firstJob", jobRepository)
                .start(firstStep())
                .build();
    }

    @Bean
    Step firstStep() {
        return new StepBuilder("firstStep", jobRepository)
                .tasklet((contribution, chunkContext) -> {
                    System.out.println("Hello, world!");
                    return RepeatStatus.FINISHED;
                }, platformTransactionManager)
                .build();
    }
}

/**
 * <3> Use an ApplicationRunner to schedule the same job in three variants:
 *      <3.1> Date object
 *      <3.2> LocalDate object
 *      <3.3> LocalDateTime object
 */
@Component
class ManualJobRunner implements ApplicationRunner {
    private final JobLocator jobLocator;
    private final JobLauncher jobLauncher;

    public ManualJobRunner(JobLocator jobLocator, JobLauncher jobLauncher) {
        this.jobLocator = jobLocator;
        this.jobLauncher = jobLauncher;
    }

    /**
     * <5> Cause the error.
     *
     * @param args incoming application arguments
     * @throws Exception Any exception during Job resultion... Won't fire here.
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        // <5.1> Find the job.
        final var job = jobLocator.getJob("firstJob");

        // <5.2> Assemble the test cases.
        final var parametersWithDate = new JobParametersBuilder().addDate("date", new Date()).toJobParameters();
        final var parametersWithLocalDate = new JobParametersBuilder()
                .addLocalDate("localDate", LocalDate.now())
                .toJobParameters();
        final var parametersWithLocalDateTime = new JobParametersBuilder()
                .addLocalDateTime("localDateTime", LocalDateTime.now())
                .toJobParameters();
        final var cases = List.of(parametersWithDate, parametersWithLocalDate, parametersWithLocalDateTime);

        // <5.3> We can observe that both LocalDate and LocalDateTime fail with the same exception:
        //
        //      No converter found capable of converting from type [java.time.LocalDate] to type [java.lang.String]
        //
        for (final var jobParameters : cases) {
            try {
                jobLauncher.run(job, jobParameters);
                System.out.println("SUCCESS: " + jobParameters);
            } catch (Throwable t) {
                System.out.println("FAILED: " + jobParameters + ", error was: " + t.getMessage());
            }
        }
    }
}
